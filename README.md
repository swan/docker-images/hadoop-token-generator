# API Docs

- POST `/generate-delegation-token`
  - Headers
    ```properties
    Authorization: token <jupyterhub user token>
    ```
  - JSON Body
    ```json
    {
        "cluster": "<cluster name>"
    }
    ```
  - Response `200`
      - Headers
        ```properties
        content-type: application/octet-stream
        ``` 
      - Body: Binary token file
  
  Example
  ```bash
  curl -X POST hadoop-token-generator:80/generate-delegation-token -H "Authorization: token $JUPYTERHUB_API_TOKEN" --data '{"cluster":"analytix"}'
  ```