FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:20240801-1

ARG JUPYTERHUB_VERSION="4.1.6"

# Install support packages
RUN dnf install -y python3-pip \
                   # needed by hadoop-fetchdt
                   which && \
    dnf clean all && rm -rf /var/cache/dnf

# Install JupyterHub
RUN pip3 install --no-cache \
         jupyterhub==${JUPYTERHUB_VERSION}

# Configure CERN's kerberos realm
ADD ./conf/krb5.conf /etc/krb5.conf

# Add Hadoop repo and install fetchdt
ADD ./repos/hdp9al-stable.repo /etc/yum.repos.d/hdp9al-stable.repo
RUN dnf -y install hadoop-fetchdt && \
    dnf clean all && rm -rf /var/cache/dnf

# Add main program and script of the hadoop token generator
ADD ./hadoop-token-generator /hadoop-token-generator
RUN chmod 544 /hadoop-token-generator/*.sh

ENTRYPOINT ["python3"]
CMD ["/hadoop-token-generator/main.py"]
