#!/bin/bash
# Located at [/hadoop-token-generator/hadoop-token.sh]

TOKEN_FILE_PATH=${1}
USER=${2}
CLUSTER=${3}

# For spark on k8s, generate hdfs tokens for the analytix cluster
if [ ${CLUSTER} = "k8s" ]; then
    CLUSTER="analytix"
fi

if [[ ! -f "/hadoop-token-generator/hadoop.cred" ]]; then
    echo "keytab file not found" >&2
    exit 1;
fi

if [ "$SWAN_DEV" = "true" ]; then
    # For dev purposes, one can provide already generated token instead of the proxy user keytab
    cat /hadoop-token-generator/hadoop.cred > "${TOKEN_FILE_PATH}"
    exit 0
fi

USER_GROUP="${USER}:def-cg"

# Generate HDFS, YARN, HIVE tokens
export KRB5CCNAME=$(mktemp /tmp/hswan.XXXXXXXXX)
LCG_VIEW=/cvmfs/sft.cern.ch/lcg/views/LCG_104a/x86_64-el9-gcc13-opt
export OVERRIDE_HADOOP_MAPRED_HOME="${LCG_VIEW}"

source "${LCG_VIEW}/setup.sh" &> /dev/null # LCG view can show some warnings due to uninstalled libraries, ignore
# HADOOP_CONF_HOME set from chart template
source "${HADOOP_CONF_HOME}/hadoop-swan-setconf.sh" "${CLUSTER}"

kinit -V -kt /hadoop-token-generator/hadoop.cred hswan@CERN.CH -c "${KRB5CCNAME}"

/usr/hdp/hadoop-fetchdt-*/hadoop-fetchdt -required hdfs,yarn -proxyuser "${USER}" -tokenfile "${TOKEN_FILE_PATH}" -awaitTokenSec 15

kdestroy -c "${KRB5CCNAME}"
