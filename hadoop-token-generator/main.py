"""An external JupyterHub service that authenticates users with JupyterHub and generates Hadoop Delegation tokens for a given user and cluster"""

import os
import asyncio
import tempfile

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application
from tornado.web import authenticated
from tornado.web import RequestHandler
from tornado.escape import json_decode
from tornado.log import app_log
from jupyterhub.services.auth import HubAuthenticated
import logging

hadoop_token_script = os.getenv(
    'HADOOP_TOKEN_SCRIPT_PATH', '/hadoop-token-generator/hadoop_token.sh')

server_port = os.getenv('HADOOP_TOKEN_GENERATOR_PORT', 8080)


class HubSparkHandler(HubAuthenticated, RequestHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Prevent tornado from logging user auth info
        app_log.setLevel(logging.INFO)

    async def generate_token(self, username: str, cluster: str) -> bytes:
        logging.info(f'Generating token for {username}, {cluster}')
        token_file_descriptor, token_file_path = tempfile.mkstemp(
            prefix=f'hadoop_{username}_', suffix='.toks')
        os.close(token_file_descriptor)

        proc = await asyncio.create_subprocess_exec(hadoop_token_script, token_file_path, username, cluster, stdout=asyncio.subprocess.PIPE)
        stdout, stderr = await proc.communicate()
        if stdout:
            logging.info(stdout.decode())
        if stderr:
            logging.error(stderr.decode())
        if not(os.path.exists(token_file_path) and os.path.getsize(token_file_path) > 0):
            raise Exception('Error generating token')

        try:
            with open(token_file_path, 'rb') as file:
                token = file.read()
                return token
        finally:
            os.remove(token_file_path)

    @authenticated
    async def post(self):

        username = self.current_user["name"]
        if not username:
            self.set_status(403)
            self.finish({"reason": "Invalid user"})
            return

        body = json_decode(self.request.body)
        if not "cluster" in body:
            self.set_status(400)
            self.finish({"reason": "Invalid cluster"})
            return

        cluster = body.get("cluster")

        output = await self.generate_token(username, cluster)
        if not output:
            self.set_status(500)
            self.finish({"reason": "Error generating token"})
            return
        self.set_status(200)
        self.set_header("content-type", "application/octet-stream")
        self.write(output)


def main():
    logging.getLogger().setLevel(logging.DEBUG)
    app = Application(
        [
            (r"/generate-delegation-token", HubSparkHandler),
        ]
    )

    http_server = HTTPServer(app)
    logging.info(f"Listening on port {server_port}")
    http_server.listen(int(server_port))
    IOLoop.current().start()


if __name__ == "__main__":
    main()
